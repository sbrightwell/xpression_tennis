#!/usr/bin/env python

#
# Tennis scoring application.  Sends plaintext commands to a Ross XPpression via both TCP socket
# (using Ross' GPIs), and also textfile.
#
# Issues to resolve:
# - Does not handle tiebreakers yet (need to go manual)
# - Current Set selection radio buttons need 'clicked' connection to update bug/ticker
# - Bug score file needs to actually exist or error occurs
# - Does not yet detect end of a set (manual selection here!)
# - Does not yet tally sets won totals (manual entry for this!)
# - Many bits of code could be refined (but they work for now!)
#
# Written (in a hurry!) by 
# Stuart Brightwell - sbrightwell@iinet.net.au - 0400 975 944
# ....

import sys
import string
import socket
import time
import os
from PyQt4 import QtCore,  QtGui
from Ui_tennis_bug import Ui_MainWindow
from Ui_tennis_bug_scripts import Ui_ConfigWindow2
import cPickle as pickle

#pickle file where we store deko IP & SOCKET info:
cgconfig = 'cg_remote_config.p'
#pickle file where we store scripts & button labels:
cgscripts = (os.getcwd() + '/button_scripts/cg_scripts.p')
#cgscripts = 'cg_scripts.p'
print cgscripts

btnscript = []
btnlabel = []

game = ("0", "15", "30", "40", "Ad")
p1_game_points = 0
p2_game_points = 0
p1_set_points = [0, 0, 0, 0, 0]
p2_set_points = [0, 0, 0, 0, 0]
current_set = 0
tie_breaker = 0

grid_buttons = 54

class BoxesForm(QtGui.QMainWindow):   
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.pushButtonBOXES1.clicked.connect(self.button_cg_recall_1)
        self.ui.pushButtonBOXES2.clicked.connect(self.button_cg_recall_2)
        self.ui.pushButtonBOXES3.clicked.connect(self.button_cg_recall_3)
        self.ui.pushButtonBOXES4.clicked.connect(self.button_cg_recall_4)
        self.ui.pushButtonBOXES5.clicked.connect(self.button_cg_recall_5)
        self.ui.pushButtonBOXES6.clicked.connect(self.button_cg_recall_6)
        self.ui.pushButtonBOXES7.clicked.connect(self.button_cg_recall_7)
        self.ui.pushButtonBOXES8.clicked.connect(self.button_cg_recall_8)
        self.ui.pushButtonBOXES9.clicked.connect(self.button_cg_recall_9)
        self.ui.pushButtonBOXES10.clicked.connect(self.button_cg_recall_10)
        self.ui.pushButtonBOXES11.clicked.connect(self.button_cg_recall_11)
        self.ui.pushButtonBOXES12.clicked.connect(self.button_cg_recall_12)
        self.ui.pushButtonBOXES13.clicked.connect(self.button_cg_recall_13)
        self.ui.pushButtonBOXES14.clicked.connect(self.button_cg_recall_14)
        self.ui.pushButtonBOXES15.clicked.connect(self.button_cg_recall_15)
        self.ui.pushButtonBOXES16.clicked.connect(self.button_cg_recall_16)
        self.ui.pushButtonBOXES17.clicked.connect(self.button_cg_recall_17)
        self.ui.pushButtonBOXES18.clicked.connect(self.button_cg_recall_18)
        self.ui.pushButtonBOXES19.clicked.connect(self.button_cg_recall_19)
        self.ui.pushButtonBOXES20.clicked.connect(self.button_cg_recall_20)
        self.ui.pushButtonBOXES21.clicked.connect(self.button_cg_recall_21)
        self.ui.pushButtonBOXES22.clicked.connect(self.button_cg_recall_22)
        self.ui.pushButtonBOXES23.clicked.connect(self.button_cg_recall_23)
        self.ui.pushButtonBOXES24.clicked.connect(self.button_cg_recall_24)
        self.ui.pushButtonBOXES25.clicked.connect(self.button_cg_recall_25)
        self.ui.pushButtonBOXES26.clicked.connect(self.button_cg_recall_26)
        self.ui.pushButtonBOXES27.clicked.connect(self.button_cg_recall_27)
        self.ui.pushButtonBOXES28.clicked.connect(self.button_cg_recall_28)
        self.ui.pushButtonBOXES29.clicked.connect(self.button_cg_recall_29)
        self.ui.pushButtonBOXES30.clicked.connect(self.button_cg_recall_30)
        self.ui.pushButtonBOXES31.clicked.connect(self.button_cg_recall_31)
        self.ui.pushButtonBOXES32.clicked.connect(self.button_cg_recall_32)
        self.ui.pushButtonBOXES33.clicked.connect(self.button_cg_recall_33)
        self.ui.pushButtonBOXES34.clicked.connect(self.button_cg_recall_34)
        self.ui.pushButtonBOXES35.clicked.connect(self.button_cg_recall_35)
        self.ui.pushButtonBOXES36.clicked.connect(self.button_cg_recall_36)
        self.ui.pushButtonBOXES37.clicked.connect(self.button_cg_recall_37)
        self.ui.pushButtonBOXES38.clicked.connect(self.button_cg_recall_38)
        self.ui.pushButtonBOXES39.clicked.connect(self.button_cg_recall_39)
        self.ui.pushButtonBOXES40.clicked.connect(self.button_cg_recall_40)
        self.ui.pushButtonBOXES41.clicked.connect(self.button_cg_recall_41)
        self.ui.pushButtonBOXES42.clicked.connect(self.button_cg_recall_42)
        self.ui.pushButtonBOXES43.clicked.connect(self.button_cg_recall_43)
        self.ui.pushButtonBOXES44.clicked.connect(self.button_cg_recall_44)
        self.ui.pushButtonBOXES45.clicked.connect(self.button_cg_recall_45)
        self.ui.pushButtonBOXES46.clicked.connect(self.button_cg_recall_46)
        self.ui.pushButtonBOXES47.clicked.connect(self.button_cg_recall_47)
        self.ui.pushButtonBOXES48.clicked.connect(self.button_cg_recall_48)
        self.ui.pushButtonBOXES49.clicked.connect(self.button_cg_recall_49)
        self.ui.pushButtonBOXES50.clicked.connect(self.button_cg_recall_50)
        self.ui.pushButtonBOXES51.clicked.connect(self.button_cg_recall_51)
        self.ui.pushButtonBOXES52.clicked.connect(self.button_cg_recall_52)
        self.ui.pushButtonBOXES53.clicked.connect(self.button_cg_recall_53)
        self.ui.pushButtonBOXES54.clicked.connect(self.button_cg_recall_54)
        self.ui.pushButtonCONNECT.clicked.connect(self.button_cg_connect)
        self.ui.pushButtonSCRIPTEDITOR.clicked.connect(self.config_window)
        self.ui.pushButtonSAVECFG.clicked.connect(self.button_saveconfig)
        self.ui.pushButtonPLAYER1_WIN.clicked.connect(self.button_player1_point)
        self.ui.pushButtonPLAYER2_WIN.clicked.connect(self.button_player2_point)
        self.ui.pushButtonWRITE.clicked.connect(self.button_bug_write)
        self.ui.pushButtonRESET.clicked.connect(self.button_reset)
        self.ui.pushButtonRESET_MATCHES.clicked.connect(self.button_reset_matches)
        self.ui.checkBox3SETS.stateChanged.connect(self.three_sets)
        self.ui.radioButtonCURRENTSET1.clicked.connect(self.setchanged)
        self.ui.radioButtonCURRENTSET2.clicked.connect(self.setchanged)
        self.ui.radioButtonCURRENTSET3.clicked.connect(self.setchanged)
        self.ui.radioButtonCURRENTSET4.clicked.connect(self.setchanged)
        self.ui.radioButtonCURRENTSET5.clicked.connect(self.setchanged)
        self.ui.pushButton_SELBUGFILE.clicked.connect(self.selectbugfile)
        self.ui.frame_BTNLABELS.hide()
        self.ui.label_TIE_BREAK.hide()
        self.ui.label_END_MATCH.hide()
        
        self.ui.actionOpen_Button_Grid.triggered.connect(self.menu_open_btn_grid)
        self.ui.actionSave_Button_Grid.triggered.connect(self.menu_save_btn_grid)
        self.ui.actionSave_Button_Grid_As.triggered.connect(self.menu_save_btn_grid_as)
        self.ui.actionOpen_Match.triggered.connect(self.menu_open_match)
        self.ui.actionSave_Match.triggered.connect(self.menu_save_match)
        self.ui.actionSave_Match_As.triggered.connect(self.menu_save_match_as)
        
    def button_cg_recall_1(self, var):
        self.button_cg_recall(1)

    def button_cg_recall_2(self, var):
        self.button_cg_recall(2)

    def button_cg_recall_3(self, var):
        self.button_cg_recall(3)

    def button_cg_recall_4(self, var):
        self.button_cg_recall(4)

    def button_cg_recall_5(self, var):
        self.button_cg_recall(5)

    def button_cg_recall_6(self, var):
        self.button_cg_recall(6)

    def button_cg_recall_7(self, var):
        self.button_cg_recall(7)

    def button_cg_recall_8(self, var):
        self.button_cg_recall(8)

    def button_cg_recall_9(self, var):
        self.button_cg_recall(9)

    def button_cg_recall_10(self, var):
        self.button_cg_recall(10)

    def button_cg_recall_11(self, var):
        self.button_cg_recall(11)

    def button_cg_recall_12(self, var):
        self.button_cg_recall(12)

    def button_cg_recall_13(self, var):
        self.button_cg_recall(13)

    def button_cg_recall_14(self, var):
        self.button_cg_recall(14)

    def button_cg_recall_15(self, var):
        self.button_cg_recall(15)

    def button_cg_recall_16(self, var):
        self.button_cg_recall(16)

    def button_cg_recall_17(self, var):
        self.button_cg_recall(17)

    def button_cg_recall_18(self, var):
        self.button_cg_recall(18)

    def button_cg_recall_19(self, var):
        self.button_cg_recall(19)
        
    def button_cg_recall_20(self, var):
        self.button_cg_recall(20)

    def button_cg_recall_21(self, var):
        self.button_cg_recall(21)

    def button_cg_recall_22(self, var):
        self.button_cg_recall(22)

    def button_cg_recall_23(self, var):
        self.button_cg_recall(23)

    def button_cg_recall_24(self, var):
        self.button_cg_recall(24)

    def button_cg_recall_25(self, var):
        self.button_cg_recall(25)

    def button_cg_recall_26(self, var):
        self.button_cg_recall(26)

    def button_cg_recall_27(self, var):
        self.button_cg_recall(27)

    def button_cg_recall_28(self, var):
        self.button_cg_recall(28)

    def button_cg_recall_29(self, var):
        self.button_cg_recall(29)

    def button_cg_recall_30(self, var):
        self.button_cg_recall(30)

    def button_cg_recall_31(self, var):
        self.button_cg_recall(31)

    def button_cg_recall_32(self, var):
        self.button_cg_recall(32)

    def button_cg_recall_33(self, var):
        self.button_cg_recall(33)

    def button_cg_recall_34(self, var):
        self.button_cg_recall(34)

    def button_cg_recall_35(self, var):
        self.button_cg_recall(35)

    def button_cg_recall_36(self, var):
        self.button_cg_recall(36)

    def button_cg_recall_37(self, var):
        self.button_cg_recall(37)

    def button_cg_recall_38(self, var):
        self.button_cg_recall(38)

    def button_cg_recall_39(self, var):
        self.button_cg_recall(39)

    def button_cg_recall_40(self, var):
        self.button_cg_recall(40)

    def button_cg_recall_41(self, var):
        self.button_cg_recall(41)

    def button_cg_recall_42(self, var):
        self.button_cg_recall(42)

    def button_cg_recall_43(self, var):
        self.button_cg_recall(43)

    def button_cg_recall_44(self, var):
        self.button_cg_recall(44)

    def button_cg_recall_45(self, var):
        self.button_cg_recall(45)

    def button_cg_recall_46(self, var):
        self.button_cg_recall(46)

    def button_cg_recall_47(self, var):
        self.button_cg_recall(47)

    def button_cg_recall_48(self, var):
        self.button_cg_recall(48)

    def button_cg_recall_49(self, var):
        self.button_cg_recall(49)

    def button_cg_recall_50(self, var):
        self.button_cg_recall(50)

    def button_cg_recall_51(self, var):
        self.button_cg_recall(51)

    def button_cg_recall_52(self, var):
        self.button_cg_recall(52)

    def button_cg_recall_53(self, var):
        self.button_cg_recall(53)

    def button_cg_recall_54(self, var):
        self.button_cg_recall(54)
        
    def selectbugfile(self):
        bugfilename = self.fileDialog = QtGui.QFileDialog.getOpenFileName(self, 'Select Bug File', (os.getcwd()))
        #self.fileDialog.show()
        if (len(bugfilename))>0:
            self.log_message('Select Bug file: %s' % bugfilename)
            self.ui.lineEditBUGFILEPATH.setText(bugfilename)
        
    def menu_open_btn_grid(self):
        #self.log_message('clicked menu open button preset')
        bgrid = self.fileDialog = QtGui.QFileDialog.getOpenFileName(self, 'Open File', (os.getcwd() + '/button_scripts'))
        #self.fileDialog.show()
        self.log_message('Open button grid: %s' % bgrid)
        cg_scripts=read_btn_scripts(bgrid)
        boxesconfig.refresh_buttons (cg_scripts)
        
    def menu_save_btn_grid(self):
        return
        
    def menu_save_btn_grid_as(self):
        global cgscripts
        bgrid = self.fileDialog = QtGui.QFileDialog.getSaveFileName(self, 'Save File As', (os.getcwd() + '/button_scripts'))
        #self.fileDialog.show()
        if len(bgrid)>1:
            cgscripts = bgrid
            self.log_message('Save button grid: %s' % cgscripts)
        boxesconfig.button_savescripts()
        return
    
    def menu_open_match(self):
	match_filename = self.fileDialog = QtGui.QFileDialog.getOpenFileName(self, 'Open Match File', (os.getcwd() + '/matches'))
	if len(match_filename)>1:
	    try:
		match_details = pickle.load (open(match_filename))
		self.ui.lineEditPLAYER1_NAME.setText(match_details[0])
		self.ui.lineEditPLAYER1_NAME_ABBRV.setText(match_details[1])
		self.ui.lineEditPLAYER2_NAME.setText(match_details[2])
		self.ui.lineEditPLAYER2_NAME_ABBRV.setText(match_details[3])
	    except pickle.UnpicklingError,  error:
		boxesform.log_message('Error reading match file.')

    def menu_save_match_as(self):
	match_details = [self.ui.lineEditPLAYER1_NAME.text(), self.ui.lineEditPLAYER1_NAME_ABBRV.text(), self.ui.lineEditPLAYER2_NAME.text(), self.ui.lineEditPLAYER2_NAME_ABBRV.text()]
	match_filename = self.fileDialog = QtGui.QFileDialog.getSaveFileName(self, 'Save Match As', (os.getcwd() + '/matches'))
	if len(match_filename)>1:
	    try:
		pickle.dump (match_details, open (match_filename, "wb"))
		boxesform.log_message('Match saved.')
	    except (pickle.PicklingError, IOError) as error:
	    #except error:
		boxesform.log_message('Error writing match file! Error: %s' % (error[1]))

    def menu_save_match(self):
        return

    def button_cg_recall(self, var):
        #This is called by each script button and sends the script to a socket conn.
        cg_host, cg_port = self.gethost()
        message=(cg_scripts[var][1])
        self.cg_xfer_sock_send(message, cg_host, cg_port)
        # Maybe we try QSignalMaper() here someday to tidy this up...

    def button_cg_connect(self):
        # establish socket connection to deko host
        cg_host, cg_port = self.gethost()
        self.cg_xfer_sock_conn(cg_host,  cg_port)

    def gethost (self):
        # read host ip and port settings and return these
        HOST = (str(self.ui.lineEditCGIP.text()))
        PORT = (int (self.ui.spinBoxPORT.value()))
        return (HOST,  PORT)

    def indicate_connected (self,  enabled, cg_ip, cg_port):
        # changes 'connect' button colour & disable / reenable the buttons
        palette = QtGui.QPalette()
        if enabled:
            brush = QtGui.QBrush(QtGui.QColor(0, 192, 0))
            self.ui.statusBar.showMessage("Connected to HOST : %s  PORT : %s" % (cg_ip,  cg_port))
        else:
            brush = QtGui.QBrush(QtGui.QColor(192, 0, 0))
            self.ui.statusBar.showMessage("Disconnected")
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        self.ui.pushButtonCONNECT.setPalette(palette)
        self.enable_buttons(enabled)

    def log_message(self, log_msg):
        # Append logging messages to our log window. Not written to file yet...
        message = self.ui.plainTextEditLOG
        log_time = str(time.strftime("%I:%M:%S"))
        message.appendPlainText('%s --> %s' % (log_time, log_msg))
        print ('%s --> %s' % (log_time, log_msg))

    def enable_buttons(self, enable):
        # function to enable / disable buttons...
        self.ui.frame_CGBUTTONS.setEnabled(enable)
        
    def cg_xfer_sock_conn(self,  cg_ip, cg_port):
        #attempt to make a socket connection to our deko
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error, msg:
            self.log_message('ERROR : %s' % msg[1])
        try:
            sock.connect((cg_ip, cg_port))
            self.indicate_connected(1,  cg_ip, cg_port)
            self.log_message('Connected to HOST: %s  PORT: %s' % (cg_ip,  cg_port))
        except socket.error, msg:
            self.log_message('ERROR : %s, HOST: %s  PORT: %s' % (msg[1],  cg_ip, cg_port))
            self.indicate_connected(0, 0, 0)

    def cg_xfer_sock_send(self, cg_msg,  cg_ip,  cg_port):
        #attempt to send our message to the deko
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((cg_ip, cg_port))
            sock.send(cg_msg + '\r\n')
            self.log_message('Sent (%s bytes) : \n%s' % ((len(cg_msg), cg_msg)))
            s = sock.recv(1024)
            self.log_message('Received %s bytes: %s' % ((len(s)), (s)))
        except socket.error, msg:
            self.log_message('ERROR SENDING DATA : %s' % (msg[1]))
            self.indicate_connected(0, 0, 0)

    def button_saveconfig(self):
    # write deko IP & port settings to our config pickle
        cg_config = {"host":(self.ui.lineEditCGIP.text()), "port":(self.ui.spinBoxPORT.value()), "path":(self.ui.lineEditBUGFILEPATH.text())}
        try:
            pickle.dump (cg_config, open (cgconfig, "wb"))
            boxesform.log_message('Config saved.')
        except pickle.PicklingError,  error:
            boxesform.log_message('Error writing config file! Error: %s' % (msg[1]))
            
    def config_window(self):
    #make our config window visible...
        self.ui.frame_BTNLABELS.show()
        boxesconfig.show()

    def update_config_txt(self, btnlabel, btnscript):
    #refresh our config window with new script & button label
        boxesconfig.ui.plainTextEditSCRIPT.setPlainText(btnscript)
        boxesconfig.ui.plainTextEditBTNLBL.setPlainText(btnlabel)

    #class MatchScore(object):
        #def __init__(self):
            #current_set = 0
        #def add_p1_point(self):
            
    def button_player1_point(self):
    # Player 1 has won a point...
        global p1_game_points
        global p2_game_points
        global p1_set_points
        global current_set
        global tie_breaker
        #0=0, 1=15, 2=30, 3=40, 4=Adv
        # TIE BREAKER SHIT HERE - NOT FINISHED!!!
        #self.get_current_set()
        if self.ui.checkBox3SETS.isChecked():
            sets_to_play = 3
        else:
            sets_to_play = 5
            
        if (current_set==(sets_to_play-1)):
            last_set = 1
        else:
            last_set = 0

        if (tie_breaker==1) and (last_set==0):
            #check for tie breaker (start of tie)
            if (int(p1_game_points)==0 and int(p2_game_points==0)):
                #first point of tie breaker - no toggle of serve here.
                p1_game_points = (int(p1_game_points) +1)
                self.toggle_serve()
            elif (((int(p1_game_points) + int(p2_game_points))-1) %2)==0:
                #odd point of game.
                p1_game_points = (int(p1_game_points) +1)
            elif (((int(p1_game_points) + int(p2_game_points))) %2)==0:
                #even point of game.
                p1_game_points = (int(p1_game_points) +1)
                if (int(p1_game_points) + int(p2_game_points)) > 1:
                    self.toggle_serve()
        else:
            if (p1_game_points <5):
                #add point to player1 if not end of game
                p1_game_points += 1
            if ((p1_game_points>3) and (p2_game_points<3)):
                #player 1 has won the set (no tie breaker)
                self.get_current_set()
                p1_set_points[current_set]+=1
                p1_game_points=0
                p2_game_points=0
                self.toggle_serve()
            elif ((p1_game_points>3) and (p2_game_points==4)):
                #player 1 has won a point in advantage
                p1_game_points -= 1
                p2_game_points -= 1
            elif ((p1_game_points==5) and (p2_game_points<5)):
                #player 1 has won a game via advantage
                self.get_current_set()
                p1_set_points[current_set]+=1
                p1_game_points=0
                p2_game_points=0
                self.toggle_serve()
    #   self.log_message("Player 1 - %s" % (game[p1_game_points]))

        if (tie_breaker==0) or (tie_breaker==1 and last_set==1):
            boxesform.ui.lineEditPLAYER1_POINTS.setText(game[int(p1_game_points)])
            boxesform.ui.lineEditPLAYER2_POINTS.setText(game[int(p2_game_points)])
        else:
            # for ties we just write points 1, 2, 3, etc...
            boxesform.ui.lineEditPLAYER1_POINTS.setText(str(p1_game_points))
            boxesform.ui.lineEditPLAYER2_POINTS.setText(str(p2_game_points))
        
        if ((p1_set_points[current_set]>5) and (p2_set_points[current_set]>5) and (tie_breaker==0)):
            #check for tie breaker & set up for it.
            tie_breaker=1
            boxesform.ui.label_TIE_BREAK.show()
            boxesform.ui.lineEditPLAYER1_POINTS.setText("0")
            boxesform.ui.lineEditPLAYER2_POINTS.setText("0")
        
        if (tie_breaker==0) and ((p1_set_points[current_set])>5) and ((p1_set_points[current_set]) - (p2_set_points[current_set])>1):
            self.log_message("End of set %s - Player 1 won set!" % (current_set+1))
            p1_sets_won = str(int(self.ui.lineEditPLAYER1_SETS.text()) +1)
            self.ui.lineEditPLAYER1_SETS.setText(p1_sets_won)
            if sets_to_play==3:
                if (int(self.ui.lineEditPLAYER1_SETS.text()))<2:
                    if (last_set==0):
                        self.advance_current_set()
                        self.setchanged()
                else:
                    self.log_message("END OF MATCH!")
                    self.ui.label_END_MATCH.show()
            elif sets_to_play==5:
                if (int(self.ui.lineEditPLAYER1_SETS.text()))<3:
                    if (last_set==0):
                        self.advance_current_set()
                        self.setchanged()
                else:
                    self.log_message("END OF MATCH!")
                    self.ui.label_END_MATCH.show()
        elif (tie_breaker==1) and (last_set==0) and ((int(p1_game_points)>6) and (int(p1_game_points) - (int(p2_game_points))>1)):
            p1_set_points[current_set]+=1
            p1_sets_won = str(int(self.ui.lineEditPLAYER1_SETS.text()) +1)
            self.ui.lineEditPLAYER1_SETS.setText(p1_sets_won)
            self.update_set_score()
            self.log_message("End of set %s - Player 1 won tie breaker set!" % (current_set +1))
            if sets_to_play==3:
                if (int(self.ui.lineEditPLAYER1_SETS.text()))<2:
                    if (last_set==0):
                        self.advance_current_set()
                        self.setchanged()
                else:
                    self.log_message("END OF MATCH!")
                    self.ui.label_END_MATCH.show()
            elif sets_to_play==5:
                if (int(self.ui.lineEditPLAYER1_SETS.text()))<3:
                    if (last_set==0):
                        self.advance_current_set()
                        self.setchanged()
                else:
                    self.log_message("END OF MATCH!")
                    self.ui.label_END_MATCH.show()
                
        if self.ui.checkBoxAUTO.isChecked():
            self.write_bug_file()

            
    def button_player2_point(self):
        global p2_game_points
        global p1_game_points
        global p2_set_points
        global current_set
        global tie_breaker
        # TIE BREAKER SHIT HERE - NOT FINISHED!!!
        #self.get_current_set()
        if self.ui.checkBox3SETS.isChecked():
            sets_to_play = 3
        else:
            sets_to_play = 5
            
        if (current_set==(sets_to_play-1)):
            last_set = 1
        else:
            last_set = 0
        
        if (tie_breaker==1) and (last_set==0):
            #check for tie breaker (start of tie)
            if (int(p2_game_points)==0 and int(p1_game_points==0)):
                #first point of tie breaker - no toggle of serve here.
                p2_game_points = (int(p2_game_points) +1)
                self.toggle_serve()
            elif (((int(p2_game_points) + int(p1_game_points))-1) %2)==0:
                #odd point of game.
                p2_game_points = (int(p2_game_points) +1)
            elif (((int(p2_game_points) + int(p1_game_points))) %2)==0:
                #even point of game.
                p2_game_points = (int(p2_game_points) +1)
                if (int(p1_game_points) + int(p2_game_points)) >1:
                    self.toggle_serve()
        else:
            if p2_game_points <5:
                #add point to player2 if not end of game
                p2_game_points += 1
            if ((p2_game_points>3) and (p1_game_points<3)):
                #player 2 has won the set (no tie breaker)
                self.get_current_set()
                p2_set_points[current_set]+=1
                p2_game_points=0
                p1_game_points=0
                self.toggle_serve()
            elif ((p2_game_points==4) and (p1_game_points>3)):
                #player 2 has won the advantage
                p2_game_points -= 1
                p1_game_points -= 1
            elif ((p2_game_points==5) and (p1_game_points<5)):
                #player 1 has won a tie breaker
                self.get_current_set()
                p2_set_points[current_set]+=1
                p2_game_points=0
                p1_game_points=0
                self.toggle_serve()
        #self.log_message("Player 2 - %s" % (game[p2_game_points]))

        if (tie_breaker==0) or (tie_breaker==1 and last_set==1):
            boxesform.ui.lineEditPLAYER1_POINTS.setText(game[int(p1_game_points)])
            boxesform.ui.lineEditPLAYER2_POINTS.setText(game[int(p2_game_points)])
        else:
            # for ties we just write points 1, 2, 3, etc...
            boxesform.ui.lineEditPLAYER1_POINTS.setText(str(p1_game_points))
            boxesform.ui.lineEditPLAYER2_POINTS.setText(str(p2_game_points))

        if ((p1_set_points[current_set]>5) and (p2_set_points[current_set]>5) and (tie_breaker==0)):
            #check for tie breaker & set up for it.
            tie_breaker=1
            boxesform.ui.label_TIE_BREAK.show()
            boxesform.ui.lineEditPLAYER1_POINTS.setText("0")
            boxesform.ui.lineEditPLAYER2_POINTS.setText("0")

        if (tie_breaker==0) and ((p2_set_points[current_set])>5) and ((p2_set_points[current_set]) - (p1_set_points[current_set])>1):
            p2_sets_won = str(int(self.ui.lineEditPLAYER2_SETS.text()) +1)
            self.ui.lineEditPLAYER2_SETS.setText(p2_sets_won)
            self.log_message("End of set %s - Player 2 won set!" % (current_set +1))
            if sets_to_play==3:
                if (int(self.ui.lineEditPLAYER2_SETS.text()))<2:
                    if (last_set==0):
                        self.advance_current_set()
                        self.setchanged()
                else:
                    self.log_message("END OF MATCH!")
                    self.ui.label_END_MATCH.show()
            elif sets_to_play==5:
                if (int(self.ui.lineEditPLAYER2_SETS.text()))<3:
                    if (last_set==0):
                        self.advance_current_set()
                        self.setchanged()
                else:
                    self.log_message("END OF MATCH!")
                    self.ui.label_END_MATCH.show()
        elif (tie_breaker==1) and (last_set==0) and ((int(p2_game_points)>6) and (int(p2_game_points) - (int(p1_game_points))>1)):
            p2_set_points[current_set]+=1
            p2_sets_won = str(int(self.ui.lineEditPLAYER2_SETS.text()) +1)
            self.ui.lineEditPLAYER2_SETS.setText(p2_sets_won)
            self.update_set_score()
            self.log_message("End of set %s - Player 2 won tie breaker set!" % (current_set +1))
            if sets_to_play==3:
                if (int(self.ui.lineEditPLAYER2_SETS.text()))<2:
                    if (last_set==0):
                        self.advance_current_set()
                        self.setchanged()
                else:
                    self.log_message("END OF MATCH!")
                    self.ui.label_END_MATCH.show()
            elif sets_to_play==5:
                if (int(self.ui.lineEditPLAYER2_SETS.text()))<3:
                    if (last_set==0):
                        self.advance_current_set()
                        self.setchanged()
                else:
                    self.log_message("END OF MATCH!")
                    self.ui.label_END_MATCH.show()

        if self.ui.checkBoxAUTO.isChecked():
            self.write_bug_file()

    def button_bug_write(self):
        self.log_message("Manually writing bug file")
        self.get_current_set()	#added after day 1
        #self.update_set_score()	#added after day 1

        self.write_bug_file()
        
    def toggle_serve(self):
        if boxesform.ui.radioButtonPLAYER1_SERVE.isChecked():
            boxesform.ui.radioButtonPLAYER1_SERVE.setChecked(0)
            boxesform.ui.radioButtonPLAYER2_SERVE.setChecked(1)
        else:
            boxesform.ui.radioButtonPLAYER1_SERVE.setChecked(1)
            boxesform.ui.radioButtonPLAYER2_SERVE.setChecked(0)
            
        self.get_current_set()
        self.update_set_score()

    def get_current_set(self):
        global current_set
        if boxesform.ui.radioButtonCURRENTSET1.isChecked():
            current_set=0
        elif boxesform.ui.radioButtonCURRENTSET2.isChecked():
            current_set=1
        elif boxesform.ui.radioButtonCURRENTSET3.isChecked():
            current_set=2
        elif boxesform.ui.radioButtonCURRENTSET4.isChecked():
            current_set=3
        else:
            current_set=4
            
    def advance_current_set(self):
        global current_set
        if current_set==0:
            boxesform.ui.radioButtonCURRENTSET1.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET2.setChecked(1)
            boxesform.ui.radioButtonCURRENTSET3.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET4.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET5.setChecked(0)
        elif current_set==1:
            boxesform.ui.radioButtonCURRENTSET1.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET2.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET3.setChecked(3)
            boxesform.ui.radioButtonCURRENTSET4.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET5.setChecked(0)
        elif current_set==2:
            boxesform.ui.radioButtonCURRENTSET1.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET2.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET3.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET4.setChecked(1)
            boxesform.ui.radioButtonCURRENTSET5.setChecked(0)
        elif current_set==3:
            boxesform.ui.radioButtonCURRENTSET1.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET2.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET3.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET4.setChecked(0)
            boxesform.ui.radioButtonCURRENTSET5.setChecked(1)

    def setchanged(self):
        global current_set
        global tie_breaker
        global p1_game_points
        global p2_game_points
        self.get_current_set()
        #if boxesform.ui.radioButtonCURRENTSET1.isChecked():
            #current_set=0
        #elif boxesform.ui.radioButtonCURRENTSET2.isChecked():
            #current_set=1
        #elif boxesform.ui.radioButtonCURRENTSET3.isChecked():
            #current_set=2
        #elif boxesform.ui.radioButtonCURRENTSET4.isChecked():
            #current_set=3
        #else:
            #current_set=4
        tie_breaker=0
        self.ui.label_TIE_BREAK.hide()
        boxesform.ui.lineEditPLAYER1_POINTS.setText("0")
        boxesform.ui.lineEditPLAYER2_POINTS.setText("0")
        if self.ui.checkBoxAUTO.isChecked():
            self.write_bug_file()
        p1_game_points=0
        p2_game_points=0


    def button_reset(self):
        self.log_message("Reset current game scores")
        global p2_game_points
        global p1_game_points
        global tie_breaker
        tie_breaker=0
        p2_game_points=0
        p1_game_points=0
        boxesform.ui.label_TIE_BREAK.hide()
        boxesform.ui.lineEditPLAYER1_POINTS.setText(game[0])
        boxesform.ui.lineEditPLAYER2_POINTS.setText(game[0])

    def button_reset_matches(self):
        self.log_message("Reset match scores")
        global p2_game_points
        global p1_game_points
        global p1_set_points
        global p2_set_points	
        global current_set
        self.ui.label_END_MATCH.hide()
        current_set = 0
        p1_set_points = [0, 0, 0, 0, 0]
        p2_set_points = [0, 0, 0, 0, 0]
        self.ui.lineEditPLAYER1_SET1.setText('0')
        self.ui.lineEditPLAYER1_SET2.setText('0')
        self.ui.lineEditPLAYER1_SET3.setText('0')
        self.ui.lineEditPLAYER1_SET4.setText('0')
        self.ui.lineEditPLAYER1_SET5.setText('0')
        self.ui.lineEditPLAYER2_SET1.setText('0')
        self.ui.lineEditPLAYER2_SET2.setText('0')
        self.ui.lineEditPLAYER2_SET3.setText('0')
        self.ui.lineEditPLAYER2_SET4.setText('0')
        self.ui.lineEditPLAYER2_SET5.setText('0')
        self.ui.lineEditPLAYER1_SETS.setText('0')
        self.ui.lineEditPLAYER2_SETS.setText('0')
        self.ui.radioButtonCURRENTSET1.setChecked(1)
        self.ui.radioButtonCURRENTSET2.setChecked(0)
        self.ui.radioButtonCURRENTSET3.setChecked(0)
        self.ui.radioButtonCURRENTSET4.setChecked(0)
        self.ui.radioButtonCURRENTSET5.setChecked(0)

    def three_sets(self, threesets):
        # This enables or disables sets 4 & 5 (for Women's games)
        if threesets:
            self.ui.frame_5SETS.setEnabled(0)
        else:
            self.ui.frame_5SETS.setEnabled(1)
 
    def update_set_score(self):
        global p1_set_points
        global p2_set_points
        if boxesform.ui.checkBoxSETLOCK_1.isChecked()==0:
            boxesform.ui.lineEditPLAYER1_SET1.setText(str (p1_set_points[0]))
            boxesform.ui.lineEditPLAYER2_SET1.setText(str (p2_set_points[0]))
        if boxesform.ui.checkBoxSETLOCK_2.isChecked()==0:
            boxesform.ui.lineEditPLAYER1_SET2.setText(str (p1_set_points[1]))
            boxesform.ui.lineEditPLAYER2_SET2.setText(str (p2_set_points[1]))
        if boxesform.ui.checkBoxSETLOCK_3.isChecked()==0:
            boxesform.ui.lineEditPLAYER1_SET3.setText(str (p1_set_points[2]))
            boxesform.ui.lineEditPLAYER2_SET3.setText(str (p2_set_points[2]))
        if boxesform.ui.checkBoxSETLOCK_4.isChecked()==0:
            boxesform.ui.lineEditPLAYER1_SET4.setText(str (p1_set_points[3]))
            boxesform.ui.lineEditPLAYER2_SET4.setText(str (p2_set_points[3]))
        if boxesform.ui.checkBoxSETLOCK_5.isChecked()==0:
            boxesform.ui.lineEditPLAYER1_SET5.setText(str (p1_set_points[4]))
            boxesform.ui.lineEditPLAYER2_SET5.setText(str (p2_set_points[4]))

    def update_sets_won(self):
        # Need to compare set scores to establish if set is over.
        return()
        #if self.ui.

    def write_bug_file(self):
        global current_set
        bug_delim = (";" + '\r\n')
        #Ticker name & sets - Player 1
        if self.ui.radioButtonPLAYER1_SERVE.isChecked():
            bug_file_data_1 = (self.ui.lineEditPLAYER1_NAME_ABBRV.text() + '*' + bug_delim + self.ui.lineEditPLAYER1_SETS.text() \
            + bug_delim)
        else:
            bug_file_data_1 = (self.ui.lineEditPLAYER1_NAME_ABBRV.text() + bug_delim + self.ui.lineEditPLAYER1_SETS.text() \
            + bug_delim)
        if current_set == 0:
            bug_file_data_2 = (self.ui.lineEditPLAYER1_SET1.text() + bug_delim)
        elif current_set == 1:
            bug_file_data_2 = (self.ui.lineEditPLAYER1_SET2.text() + bug_delim)
        elif current_set == 2:
            bug_file_data_2 = (self.ui.lineEditPLAYER1_SET3.text() + bug_delim)
        elif current_set == 3:
            bug_file_data_2 = (self.ui.lineEditPLAYER1_SET4.text() + bug_delim)
        elif current_set == 4:
            bug_file_data_2 = (self.ui.lineEditPLAYER1_SET5.text() + bug_delim)
        bug_file_data_3 = (self.ui.lineEditPLAYER1_POINTS.text() + bug_delim)

        #Ticker name & sets - Player 2
        if self.ui.radioButtonPLAYER2_SERVE.isChecked():
            bug_file_data_4 = (self.ui.lineEditPLAYER2_NAME_ABBRV.text() + '*' + bug_delim + self.ui.lineEditPLAYER2_SETS.text() \
            + bug_delim)
        else:
            bug_file_data_4 = (self.ui.lineEditPLAYER2_NAME_ABBRV.text() + bug_delim + self.ui.lineEditPLAYER2_SETS.text() \
            + bug_delim)
        if current_set == 0:
            bug_file_data_5 = (self.ui.lineEditPLAYER2_SET1.text() + bug_delim)
        elif current_set == 1:
            bug_file_data_5 = (self.ui.lineEditPLAYER2_SET2.text() + bug_delim)
        elif current_set == 2:
            bug_file_data_5 = (self.ui.lineEditPLAYER2_SET3.text() + bug_delim)
        elif current_set == 3:
            bug_file_data_5 = (self.ui.lineEditPLAYER2_SET4.text() + bug_delim)
        elif current_set == 4:
            bug_file_data_5 = (self.ui.lineEditPLAYER2_SET5.text() + bug_delim)
        bug_file_data_6 = (self.ui.lineEditPLAYER2_POINTS.text() + bug_delim)

        bug_file_data_7 = (self.ui.lineEditPLAYER1_NAME.text() + bug_delim + self.ui.lineEditPLAYER1_SET1.text() + bug_delim \
        + self.ui.lineEditPLAYER1_SET2.text() + bug_delim + self.ui.lineEditPLAYER1_SET3.text() + bug_delim \
        + self.ui.lineEditPLAYER1_SET4.text() + bug_delim + self.ui.lineEditPLAYER1_SET5.text() + bug_delim)
        bug_file_data_8 = (self.ui.lineEditPLAYER2_NAME.text() + bug_delim + self.ui.lineEditPLAYER2_SET1.text() + bug_delim \
        + self.ui.lineEditPLAYER2_SET2.text() + bug_delim + self.ui.lineEditPLAYER2_SET3.text() + bug_delim \
        + self.ui.lineEditPLAYER2_SET4.text() + bug_delim + self.ui.lineEditPLAYER2_SET5.text() + bug_delim)

        if self.ui.radioButtonPLAYER1_SERVE.isChecked():
            #put our serve flag at the end
            bug_file_data_9 = "1" + bug_delim
        else:
            bug_file_data_9 = "2" + bug_delim

        bug_file_data = (bug_file_data_1 + bug_file_data_2 + bug_file_data_3 + bug_file_data_4 \
        + bug_file_data_5 + bug_file_data_6 + bug_file_data_7 + bug_file_data_8 + bug_file_data_9)

        bug_file = self.ui.lineEditBUGFILEPATH.text()
        try:
            with open (bug_file, 'w') as f:
                f.write(bug_file_data)
            #self.log_message('bug data file = %s' % bug_file)
        except IOError, error:
            self.log_message('Error writing bug data file: %s' % error[1])

def read_btn_scripts(cgscripts):
    # read the script files into a list called btnscript
    try:
        cg_scripts = pickle.load (open(cgscripts))
    except pickle.UnpicklingError,  error:
        boxesform.log_message('Error reading scripts!')
            
    if (len(cg_scripts) < grid_buttons):
        #populate with 'empty' if we've changed number of buttons...
        for i in range((len(cg_scripts)),grid_buttons+1):
            cg_scripts.update ({(i) : ["empty","script"]})

    update_btns_and_scripts(cg_scripts)
    return cg_scripts

def update_btns_and_scripts(cg_scripts):
    # refresh labels on all buttons with new labels
    # also refresh our combobox list
    current_edit = (boxesconfig.ui.comboBoxBTNEDIT.currentIndex() + 1)
    
    for k, v in cg_scripts.items():
        if (k < (grid_buttons + 1)):
            btnlabel.append (cg_scripts[k][0])
            btnscript.append (cg_scripts[k][1])
            boxesconfig.ui.comboBoxBTNEDIT.addItem ('%s - %s' % (k, cg_scripts[k][0].replace('\n', ' ')))

    boxesform.update_config_txt(btnlabel[0], btnscript[0])
    boxesform.three_sets(boxesform.ui.checkBox3SETS.checkState())

def read_config(configfile):
    # read the config data (port & host addr) & assign values to our form.
    try:
        cg_config = pickle.load (open(configfile))
        boxesform.ui.lineEditBUGFILEPATH.setText(cg_config["path"])
        boxesform.ui.lineEditCGIP.setText(cg_config["host"])
        boxesform.ui.spinBoxPORT.setValue(int (cg_config["port"]))
    except pickle.UnpicklingError,  error:
        boxesform.log_message('Error reading config file. SET HOST & IP SETTINGS MANUALLY!')
        boxesform.ui.lineEditBUGFILEPATH.setText("Path to XPression bug here")
        boxesform.ui.lineEditCGIP.setText('localhost')
        boxesform.ui.spinBoxPORT.setValue(51423)

class BoxesConfig(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_ConfigWindow2()
        self.ui.setupUi(self)
        self.ui.pushButtonCLOSE.clicked.connect(self.button_close)
        self.ui.pushButtonSAVESCRIPTS.clicked.connect(self.button_savescripts)
        self.ui.comboBoxBTNEDIT.activated.connect(self.btnedit_changed)
       
    def closeEvent(self, event):
        #detects when user hits X to close script editor window!
        self.button_close()

    def button_savescripts(self):
        global cgscripts
        # write newly edited script/button label to memory & pickle it
        current_edit = (self.ui.comboBoxBTNEDIT.currentIndex() + 1)
        (cg_scripts[current_edit][1]) = str(self.ui.plainTextEditSCRIPT.toPlainText())
        (cg_scripts[current_edit][0]) = str(self.ui.plainTextEditBTNLBL.toPlainText())
        #boxesform.log_message (cg_scripts[current_edit][0])
        combotext = (("%s - %s") % (str(current_edit), (cg_scripts[current_edit][0].replace('\n', ' '))))
        self.ui.comboBoxBTNEDIT.setItemText((current_edit - 1), combotext)
        self.refresh_buttons (cg_scripts)
        try:
            pickle.dump (cg_scripts, open (cgscripts, "wb"))
            boxesform.log_message('Scripts saved.')
        except (pickle.PicklingError, IOError) as error:
        #except error:
            boxesform.log_message('Error writing scripts file! Error: %s' % (error[1]))

    def button_close(self):
        boxesform.ui.frame_BTNLABELS.hide()
        self.close()

    def btnedit_changed(self):
	# refresh script / button label editor when user changes combobox
	current_edit = (self.ui.comboBoxBTNEDIT.currentIndex() + 1)
	self.ui.plainTextEditBTNLBL.setPlainText(cg_scripts[current_edit][0])
	self.ui.plainTextEditSCRIPT.setPlainText(cg_scripts[current_edit][1])
	self.refresh_buttons (cg_scripts)

    def refresh_buttons(self, cg_scripts):
	# refresh all our buttons.  Messy...
	boxesform.ui.pushButtonBOXES1.setText(cg_scripts[1][0])
	boxesform.ui.pushButtonBOXES2.setText(cg_scripts[2][0])
	boxesform.ui.pushButtonBOXES3.setText(cg_scripts[3][0])
	boxesform.ui.pushButtonBOXES4.setText(cg_scripts[4][0])
	boxesform.ui.pushButtonBOXES5.setText(cg_scripts[5][0])
	boxesform.ui.pushButtonBOXES6.setText(cg_scripts[6][0])
	boxesform.ui.pushButtonBOXES7.setText(cg_scripts[7][0])
	boxesform.ui.pushButtonBOXES8.setText(cg_scripts[8][0])
	boxesform.ui.pushButtonBOXES9.setText(cg_scripts[9][0])
	boxesform.ui.pushButtonBOXES10.setText(cg_scripts[10][0])
	boxesform.ui.pushButtonBOXES11.setText(cg_scripts[11][0])
	boxesform.ui.pushButtonBOXES12.setText(cg_scripts[12][0])
	boxesform.ui.pushButtonBOXES13.setText(cg_scripts[13][0])
	boxesform.ui.pushButtonBOXES14.setText(cg_scripts[14][0])
	boxesform.ui.pushButtonBOXES15.setText(cg_scripts[15][0])
	boxesform.ui.pushButtonBOXES16.setText(cg_scripts[16][0])
	boxesform.ui.pushButtonBOXES17.setText(cg_scripts[17][0])
	boxesform.ui.pushButtonBOXES18.setText(cg_scripts[18][0])
	boxesform.ui.pushButtonBOXES19.setText(cg_scripts[19][0])
	boxesform.ui.pushButtonBOXES20.setText(cg_scripts[20][0])
	boxesform.ui.pushButtonBOXES21.setText(cg_scripts[21][0])
	boxesform.ui.pushButtonBOXES22.setText(cg_scripts[22][0])
	boxesform.ui.pushButtonBOXES23.setText(cg_scripts[23][0])
	boxesform.ui.pushButtonBOXES24.setText(cg_scripts[24][0])
	boxesform.ui.pushButtonBOXES25.setText(cg_scripts[25][0])
	boxesform.ui.pushButtonBOXES26.setText(cg_scripts[26][0])
	boxesform.ui.pushButtonBOXES27.setText(cg_scripts[27][0])
	boxesform.ui.pushButtonBOXES28.setText(cg_scripts[28][0])
	boxesform.ui.pushButtonBOXES29.setText(cg_scripts[29][0])
	boxesform.ui.pushButtonBOXES30.setText(cg_scripts[30][0])
	boxesform.ui.pushButtonBOXES31.setText(cg_scripts[31][0])
	boxesform.ui.pushButtonBOXES32.setText(cg_scripts[32][0])
	boxesform.ui.pushButtonBOXES33.setText(cg_scripts[33][0])
	boxesform.ui.pushButtonBOXES34.setText(cg_scripts[34][0])
	boxesform.ui.pushButtonBOXES35.setText(cg_scripts[35][0])
	boxesform.ui.pushButtonBOXES36.setText(cg_scripts[36][0])
	boxesform.ui.pushButtonBOXES37.setText(cg_scripts[37][0])
	boxesform.ui.pushButtonBOXES38.setText(cg_scripts[38][0])
	boxesform.ui.pushButtonBOXES39.setText(cg_scripts[39][0])
	boxesform.ui.pushButtonBOXES40.setText(cg_scripts[40][0])
	boxesform.ui.pushButtonBOXES41.setText(cg_scripts[41][0])
	boxesform.ui.pushButtonBOXES42.setText(cg_scripts[42][0])
	boxesform.ui.pushButtonBOXES43.setText(cg_scripts[43][0])
	boxesform.ui.pushButtonBOXES44.setText(cg_scripts[44][0])
	boxesform.ui.pushButtonBOXES45.setText(cg_scripts[45][0])
	boxesform.ui.pushButtonBOXES46.setText(cg_scripts[46][0])
	boxesform.ui.pushButtonBOXES47.setText(cg_scripts[47][0])
	boxesform.ui.pushButtonBOXES48.setText(cg_scripts[48][0])
	boxesform.ui.pushButtonBOXES49.setText(cg_scripts[49][0])
	boxesform.ui.pushButtonBOXES50.setText(cg_scripts[50][0])
	boxesform.ui.pushButtonBOXES51.setText(cg_scripts[51][0])
	boxesform.ui.pushButtonBOXES52.setText(cg_scripts[52][0])
	boxesform.ui.pushButtonBOXES53.setText(cg_scripts[53][0])
	boxesform.ui.pushButtonBOXES54.setText(cg_scripts[54][0])
	
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    boxesform = BoxesForm()
    boxesconfig = BoxesConfig()
    boxesform.ui.tabWidget.setCurrentIndex(0)
    read_config(cgconfig)
    cg_scripts = read_btn_scripts(cgscripts)
    boxesconfig.refresh_buttons (cg_scripts) ###
    boxesform.indicate_connected(0, 0, 0)
    boxesform.button_reset
    boxesform.show()
    sys.exit(app.exec_())

