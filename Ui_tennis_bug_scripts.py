# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/stuart/src/kdevelop projects/xpression_tennis/tennis_bug_scripts.ui'
#
# Created: Tue Nov 19 21:57:06 2013
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ConfigWindow2(object):
    def setupUi(self, ConfigWindow2):
        ConfigWindow2.setObjectName(_fromUtf8("ConfigWindow2"))
        ConfigWindow2.resize(665, 289)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("TA_icon.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        ConfigWindow2.setWindowIcon(icon)
        self.centralwidget = QtGui.QWidget(ConfigWindow2)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.frame = QtGui.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(0, 0, 661, 281))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.comboBoxBTNEDIT = QtGui.QComboBox(self.frame)
        self.comboBoxBTNEDIT.setGeometry(QtCore.QRect(10, 12, 311, 41))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.comboBoxBTNEDIT.setFont(font)
        self.comboBoxBTNEDIT.setMaxVisibleItems(17)
        self.comboBoxBTNEDIT.setObjectName(_fromUtf8("comboBoxBTNEDIT"))
        self.pushButtonSAVESCRIPTS = QtGui.QPushButton(self.frame)
        self.pushButtonSAVESCRIPTS.setGeometry(QtCore.QRect(448, 12, 91, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(192, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(192, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(192, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        self.pushButtonSAVESCRIPTS.setPalette(palette)
        self.pushButtonSAVESCRIPTS.setObjectName(_fromUtf8("pushButtonSAVESCRIPTS"))
        self.pushButtonCLOSE = QtGui.QPushButton(self.frame)
        self.pushButtonCLOSE.setGeometry(QtCore.QRect(558, 12, 91, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(192, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(192, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(192, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        self.pushButtonCLOSE.setPalette(palette)
        self.pushButtonCLOSE.setObjectName(_fromUtf8("pushButtonCLOSE"))
        self.frame_2 = QtGui.QFrame(self.frame)
        self.frame_2.setGeometry(QtCore.QRect(9, 60, 641, 211))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.label = QtGui.QLabel(self.frame_2)
        self.label.setGeometry(QtCore.QRect(22, -5, 61, 61))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.kcolorbutton = KColorButton(self.frame_2)
        self.kcolorbutton.setGeometry(QtCore.QRect(15, 180, 71, 24))
        self.kcolorbutton.setObjectName(_fromUtf8("kcolorbutton"))
        self.plainTextEditSCRIPT = QtGui.QPlainTextEdit(self.frame_2)
        self.plainTextEditSCRIPT.setGeometry(QtCore.QRect(110, 30, 521, 171))
        self.plainTextEditSCRIPT.setObjectName(_fromUtf8("plainTextEditSCRIPT"))
        self.plainTextEditBTNLBL = QtGui.QPlainTextEdit(self.frame_2)
        self.plainTextEditBTNLBL.setGeometry(QtCore.QRect(10, 80, 81, 71))
        self.plainTextEditBTNLBL.setFrameShape(QtGui.QFrame.StyledPanel)
        self.plainTextEditBTNLBL.setFrameShadow(QtGui.QFrame.Raised)
        self.plainTextEditBTNLBL.setLineWidth(1)
        self.plainTextEditBTNLBL.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.plainTextEditBTNLBL.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.plainTextEditBTNLBL.setBackgroundVisible(False)
        self.plainTextEditBTNLBL.setObjectName(_fromUtf8("plainTextEditBTNLBL"))
        self.label_2 = QtGui.QLabel(self.frame_2)
        self.label_2.setGeometry(QtCore.QRect(107, 9, 61, 21))
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.frame_2)
        self.label_3.setGeometry(QtCore.QRect(20, 57, 61, 21))
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.frame_2)
        self.label_4.setGeometry(QtCore.QRect(20, 160, 61, 21))
        self.label_4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        ConfigWindow2.setCentralWidget(self.centralwidget)

        self.retranslateUi(ConfigWindow2)
        QtCore.QMetaObject.connectSlotsByName(ConfigWindow2)
        ConfigWindow2.setTabOrder(self.comboBoxBTNEDIT, self.pushButtonSAVESCRIPTS)

    def retranslateUi(self, ConfigWindow2):
        ConfigWindow2.setWindowTitle(_translate("ConfigWindow2", "Script & Button Configuration", None))
        self.pushButtonSAVESCRIPTS.setText(_translate("ConfigWindow2", "Save", None))
        self.pushButtonCLOSE.setText(_translate("ConfigWindow2", "Close", None))
        self.label.setText(_translate("ConfigWindow2", "<html><head/><body><p align=\"center\">Button<br/>Editor</p></body></html>", None))
        self.label_2.setText(_translate("ConfigWindow2", "<html><head/><body><p align=\"center\">Script</p></body></html>", None))
        self.label_3.setText(_translate("ConfigWindow2", "<html><head/><body><p align=\"center\">Label</p></body></html>", None))
        self.label_4.setText(_translate("ConfigWindow2", "<html><head/><body><p align=\"center\">Colour</p></body></html>", None))

from PyKDE4.kdeui import KColorButton
